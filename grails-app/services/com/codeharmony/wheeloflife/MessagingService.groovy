package com.codeharmony.wheeloflife

import grails.transaction.Transactional

@Transactional
class MessagingService {

    def mailService

    def sendWheelOfLife(Project project, projectLink,path){
        try{
            if(project && project.emailSent==false){
                def receiver = project?.visitor?.email
                def sender = mailService.mailConfig.username



                mailService.sendMail {
                    multipart true
                    async true
                    to receiver
                    from sender
                    subject "Your Wheel of Life is ready"
                    html "Hey,<br><br> Your Wheel of Life is ready and you can find it here: <a href='${projectLink}'>Go to Wheel of Life</a> "
                    attachBytes "my_wheel_of_life.pdf",'application/pdf', new File(path).readBytes()
                }
            }
        }catch(e){
            log.error(e)
        }
    }
}
