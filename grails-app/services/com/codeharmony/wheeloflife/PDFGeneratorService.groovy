package com.codeharmony.wheeloflife

import grails.transaction.Transactional
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream
import org.apache.pdfbox.pdmodel.font.PDFont
import org.apache.pdfbox.pdmodel.font.PDType1Font
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

@Transactional
class PDFGeneratorService {

    def saveWheelOfLifeToPDF(String fileLocation, Project projectInstance, String imageDataBase64String) {

        String imageFileName = fileLocation  + projectInstance?.url + ".png"
        String pdfFileName = fileLocation  + projectInstance?.url + ".pdf"
        byte[] decodedBytes = imageDataBase64String.decodeBase64()
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes))

        if (image == null) {
            log.error("Buffered Image is null")
        }

        File file = new File(imageFileName)
        ImageIO.write(image, "png", file)

        PDDocument document = new PDDocument()
        PDPage page = new PDPage()
        document.addPage(page)

        BufferedImage inputStream = ImageIO.read(new File(imageFileName))
        PDPixelMap imageBase64 = new PDPixelMap(document, inputStream)
        PDPageContentStream contentStream = new PDPageContentStream(document, page)


        //Get page height and width default is A4 size
        float pageHeight = page.getMediaBox().getHeight()
        float pageWidth = page.getMediaBox().getWidth()

        // Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.HELVETICA_BOLD
        PDFont normalFont = PDType1Font.HELVETICA

        int marginTop = 30

        // Title related configuration
        String title = "My Wheel Of Life"
        int titleFontSize = 30
        float titleWidth = font.getStringWidth(title) / 1000 * titleFontSize
        float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * titleFontSize
        float xTitle = (pageWidth - titleWidth) / 2
        float yTitle = pageHeight - marginTop - titleHeight


        // Date related configuration
        String date = projectInstance?.lastUpdated.format('dd MMM yyyy')
        int dateFontSize = 15
        float dateWidth = font.getStringWidth(date) / 1000 * dateFontSize
        float dateHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * dateFontSize
        float xDate = (pageWidth - dateWidth) / 2
        float yDate = yTitle - dateHeight - 10

        // Wheel of Life chart related configuration
        float xImage = (pageWidth - imageBase64.width) / 2
        float yImage = yDate - imageBase64.height - 20

        //
        String rateYourLifeMessage = "Rate you life for free at http://wheeloflife.dgmates.com"
        int rateYourLifeMessageFontSize = 15
        float rateYourLifeMessageWidth = font.getStringWidth(rateYourLifeMessage) / 1000 * rateYourLifeMessageFontSize
        float rateYourLifeMessageHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * rateYourLifeMessageFontSize
        float xRateYourLifeMessage = (pageWidth - rateYourLifeMessageWidth) / 2
        float yRateYourLifeMessage = yImage - rateYourLifeMessageHeight - 50

        contentStream.beginText()
        contentStream.setFont(font, titleFontSize)
        contentStream.moveTextPositionByAmount(xTitle, yTitle)
        contentStream.drawString(title)
        contentStream.endText()

        contentStream.beginText()
        contentStream.setFont(normalFont, dateFontSize)
        contentStream.moveTextPositionByAmount(xDate, yDate)
        contentStream.drawString(date)
        contentStream.endText()

        contentStream.beginText()
        contentStream.setFont(normalFont, rateYourLifeMessageFontSize)
        contentStream.moveTextPositionByAmount(xRateYourLifeMessage, yRateYourLifeMessage)
        contentStream.drawString(rateYourLifeMessage)
        contentStream.endText()


        contentStream.drawImage(imageBase64,xImage, yImage)

        // Make sure that the content stream is closed:
        contentStream.close()

        // Save the results and ensure that the document is properly closed:
        document.save(pdfFileName)
        document.close()
    }
}
