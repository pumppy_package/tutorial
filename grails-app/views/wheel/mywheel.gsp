<%--
  Created by IntelliJ IDEA.
  User: bkpandey
  Date: 7/31/15
  Time: 6:16 PM
--%>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="layout" content="front"/>

    <meta property="og:url"           content="${createLink(controller: 'wheel', action: 'mywheel', id: projectInstance?.url, absolute: true)}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="I just rated my life! Where do you think I need help?" />
    <meta property="og:description"   content="Rate your own life now - it's free!" />
    <meta property="og:image"         content="${grailsApplication.config.wheeloflife.serverUrl}${projectInstance?.url}.png" />
    <meta property="og:image:width"   content="1260">
    <meta property="og:image:height"  content="630">

    <title>The Ultimate Wheel of Life Interactive Assessment</title>

</head>

<body>
<!-- Jumbotron -->
<div class="sidebar-brand col-md-2"></div>
<div class="jumbotron col-md-7">
    <h2>${projectInstance?.visitor?.firstName}'s Wheel of Life</h2>
    <p class="lead"><g:formatDate date="${projectInstance?.lastUpdated}" type="date" style="MEDIUM"/></p>
    <canvas id="skills" width="400" height="400"></canvas>

    <g:form controller="wheel" action="saveAsPDF" id="${projectInstance}">
        <img id="url" hidden="true"/>
        <input type="text" id="imageUrl" name="imageUrl" hidden="true"/>
       %{-- <input type="submit" value="Save as PDF" class="btn btn-link btn-success"/>--}%
    </g:form>

    <script type="application/javascript">

        var data = ${raw(jsonProjectInstance)}

        var context = document.getElementById('skills').getContext('2d');

        var options =
        {
            onAnimationComplete: function()
            {
                this.showTooltip(this.segments, true);
                done()
            },
            tooltipEvents: [],
            showTooltips: true,
            scaleShowLabels:false
        }

        var myPolarAreaChart = new Chart(context).PolarArea(data, options);

        function done() {
            console.log('done');
            var url=document.getElementById("skills").toDataURL();
            document.getElementById("url").src=url;
            document.getElementById("imageUrl").value=url;
            //sendAlert(url);

        }

        var sendAlert = function(imageData){
            $.ajax({
                url: '${createLink(controller:"wheel",action:"triggerEmail")}',
                method:'POST',
                data: {
                    id: '${projectInstance.url}',
                    imageUrl:imageData
                },
                context: document.body
            }).done(function(result){
            });
            return false;
        }


    </script>

</div>

<div class="sidebar-brand col-md-3 ">
    %{--<div id="socialmedia">
        <div id="linkedin" data-url="${createLink(controller: 'wheel', action: 'mywheel', id: projectInstance?.url, absolute: true)}" data-text="LinkedIn Share"></div>
        <div id="twitter" data-url="${createLink(controller: 'wheel', action: 'mywheel', id: projectInstance?.url, absolute: true)}" data-text="Twitter Share"></div>
        <div id="facebook" data-url="${createLink(controller: 'wheel', action: 'mywheel', id: projectInstance?.url, absolute: true)}" data-text="Facebook Share"></div>
    </div>--}%
</div>

</body></html>

