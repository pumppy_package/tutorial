<%--
  Created by IntelliJ IDEA.
  User: bkpandey
  Date: 8/4/15
  Time: 9:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>User Register</title>
    <meta name="layout" content="front" />
</head>

<body>

<div class="jumbotron">
    <h2>Personal Information!</h2>
    <div class="row" >



        <div class="col-md-2"></div>
        <div class="col-md-8">
            <g:if test="${flash.message}">
                <div data-alert="alert" class="alert alert-danger fade in">
                    <p>${flash.message}</p>
                </div>
            </g:if>
            <div class="panel panel-info">
                <div class="panel-heading">We need these information</div>
                <div class="panel-body">
                    <g:form controller="wheel" action="save">

                        <div class="form-group">
                            <label for="firstName" class="control-label">First Name</label>
                            <input type="text" placeholder="Enter First Name" id="firstName" name="firstName" class="form-control" required="true">
                        </div>

                        <div class="form-group">
                            <label for="lastName" class="control-label">Last Name</label>
                            <input type="text" placeholder="Enter Last Name" id="lastName" name="lastName" class="form-control" required="true">
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" placeholder="Enter email" id="email" name="email" class="form-control" required="true">
                        </div>

                        <button class="btn btn-default" type="submit">Continue...</button>

                    </g:form>

                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
</body>
</html>