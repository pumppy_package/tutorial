<%--
  Created by IntelliJ IDEA.
  User: bkpandey
  Date: 7/31/15
  Time: 6:16 PM
--%>


<!DOCTYPE html>
<html lang="en"><head>
    <meta name="layout" content="front"/>

    <title>The Ultimate Wheel of Life Interactive Assessment</title>

</head>

<body>
<!-- Jumbotron -->
<div class="jumbotron">
    <h1>${ratingAreaInstance?.label}</h1>
    <p class="lead">How would you rate this area of your life?</p>

        <div class="row col-md-12">
            <div class="col-md-1"></div>
            <g:each in="${1..10}" status="i" var="rating">
                <div class="col-md-1">
                    <g:form action="addRatingToAreaOfLife" id="${rating}" params="[ratingAreaInstance: ratingAreaInstance?.id]">
                        <input value="${projectInstance}" name="projectInstance" type="hidden" />
                        <button type="submit" class="btn btn-lg btn-default" type="button">${rating}</button>
                    </g:form>
                </div>
            </g:each>
            <div class="col-md-1"></div>
        </div>

        <g:form action="addRatingToAreaOfLife" id="isskip" params="[ratingAreaInstance: ratingAreaInstance?.id]">
            <input value="${projectInstance}" name="projectInstance" type="hidden" />
            <button type="submit" class="btn btn-lg btn-link" type="button">Skip this area - it's not important to me</button>
        </g:form>



    <h2>Preview</h2>

    <canvas id="skills" width="250" height="250"></canvas>

    <script type="application/javascript">

        var data = ${raw(jsonProjectInstance)}


        var context = document.getElementById('skills').getContext('2d');

        var skillsChart =  new Chart(context).PolarArea(data, {
            animationEasing : "easeOutBounce"
        });

    </script>

</div>

</body></html>

