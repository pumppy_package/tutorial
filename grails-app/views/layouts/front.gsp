<%--
  Created by IntelliJ IDEA.
  User: bkpandey
  Date: 8/4/15
  Time: 5:20 PM
--%>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <asset:stylesheet src="bootstrap.css"/>
    <asset:stylesheet src="sticky-footer-navbar.css"/>
    <asset:stylesheet src="justified-nav.css"/>
    <asset:stylesheet src="simple-sidebar.css"/>
    <asset:stylesheet src="socialmedia.css"/>
    <asset:javascript src="Chart.js"/>

    <style>
        body {
            background-image:url('${resource(dir: "images", file: "mountainnew.jpg")}');
        }
    </style>

    <g:layoutHead/>
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=685202541624475";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${createLink(uri: '/')}">Wheel of Life</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="${createLink(uri: '/')}">Home</a></li>
                <li><a href="#about">About</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    <g:layoutBody/>

</div> <!-- /container -->

<!-- Site footer -->

%{--<footer class="footer">--}%
    %{--<div class="container">--}%
        %{--<p class="text-muted">© Company 2015</p>--}%
    %{--</div>--}%
%{--</footer>--}%


<asset:javascript src="jquery.js"/>
<asset:javascript src="bootstrap.js"/>
<asset:javascript src="ie10-viewport-bug-workaround.js"/>
<asset:javascript src="jquery.sharrre.min.js"/>


<script>
    $('#twitter').sharrre({
        share: {
            twitter: true
        },
        template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>Tweet</div></a>',
        enableHover: false,
        enableTracking: true,
        buttons: { twitter: {via: '_JulienH'}},
        click: function(api, options){
            api.simulateClick();
            api.openPopup('twitter');
        }
    });
    $('#facebook').sharrre({
        share: {
            facebook: true
        },
        template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>Like</div></a>',
        enableHover: false,
        enableTracking: true,
        click: function(api, options){
            api.simulateClick();
            api.openPopup('facebook');
        }
    });
    $('#googleplus').sharrre({
        share: {
            googlePlus: true
        },
        template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>Google+</div></a>',
        enableHover: false,
        enableTracking: true,
        click: function(api, options){
            api.simulateClick();
            api.openPopup('googlePlus');
        }
    });
    $('#linkedin').sharrre({
        share: {
            linkedin: true
        },
        template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>Share</div></a>',
        enableHover: false,
        enableTracking: true,
        click: function(api, options){
            api.simulateClick();
            api.openPopup('linkedin');
        }
    });
</script>

<asset:javascript src="ekko-lightbox.js" />

<script type="text/javascript">
    $(document).ready(function ($) {
        // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function() {
                    if (window.console) {
                        return console.log('Checking our the events huh?');
                    }
                },
                onNavigate: function(direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                    }
                }
            });
        });

        //Programatically call
        $('#open-image').click(function (e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });
        $('#open-youtube').click(function (e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });

        // navigateTo
        $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function() {

                    var a = this.modal_content.find('.modal-footer a');
                    if(a.length > 0) {

                        a.click(function(e) {

                            e.preventDefault();
                            this.navigateTo(2);

                        }.bind(this));

                    }

                }
            });
        });


    });
</script>

</body>
</html>
