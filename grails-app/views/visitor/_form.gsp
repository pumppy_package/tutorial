<%@ page import="com.codeharmony.wheeloflife.Visitor" %>



<div class="fieldcontain ${hasErrors(bean: visitorInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="visitor.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" required="" value="${visitorInstance?.firstName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: visitorInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="visitor.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" required="" value="${visitorInstance?.lastName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: visitorInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="visitor.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${visitorInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: visitorInstance, field: 'project', 'error')} ">
	<label for="project">
		<g:message code="visitor.project.label" default="Project" />
		
	</label>
	<g:select id="project" name="project.id" from="${com.codeharmony.wheeloflife.Project.list()}" optionKey="id" value="${visitorInstance?.project?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

