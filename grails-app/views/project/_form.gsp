<%@ page import="com.codeharmony.wheeloflife.Project" %>



<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'areaOfLifes', 'error')} ">
	<label for="areaOfLifes">
		<g:message code="project.areaOfLifes.label" default="Area Of Lifes" />
		
	</label>
	<g:select name="areaOfLifes" from="${com.codeharmony.wheeloflife.AreaOfLife.list()}" multiple="multiple" optionKey="id" size="5" value="${projectInstance?.areaOfLifes*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'createdDate', 'error')} required">
	<label for="createdDate">
		<g:message code="project.createdDate.label" default="Created Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${projectInstance?.createdDate}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'url', 'error')} required">
	<label for="url">
		<g:message code="project.url.label" default="Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="url" required="" value="${projectInstance?.url}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'visitor', 'error')} required">
	<label for="visitor">
		<g:message code="project.visitor.label" default="Visitor" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="visitor" name="visitor.id" from="${com.codeharmony.wheeloflife.Visitor.list()}" optionKey="id" required="" value="${projectInstance?.visitor?.id}" class="many-to-one"/>

</div>

