<!DOCTYPE html>
<html lang="en"><head>
    <meta name='layout' content='front'/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Sign in to WheelOflife!</title>

    <!-- Bootstrap core CSS -->
        <!-- Custom styles for this template -->
    <asset:stylesheet src="signin.css" />

</head>

<body>

<div class="container">
    <g:if test='${flash.message}'>
        <div class='login_message'>${flash.message}</div>
    </g:if>

    <form action='${postUrl}'  method='POST' id='loginForm' autocomplete="off" class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="username" class="sr-only">User Name</label>
        <input type='text' class='form-control' placeholder="Username" required="" autofocus="" name='j_username' id='username'/>
        <label for="password" class="sr-only">Password</label>
        <input name='j_password' id='password' class="form-control" placeholder="Password" required="" type="password">
        <div class="checkbox">
            <label>
                <input type='checkbox' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>  Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div> <!-- /container -->

<script type='text/javascript'>
    (function() {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
</script>


</body></html>