<%@ page import="com.codeharmony.wheeloflife.Rating" %>



<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'value', 'error')} required">
	<label for="value">
		<g:message code="rating.value.label" default="Value" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="value" from="${ratingInstance.constraints.value.inList}" required="" value="${fieldValue(bean: ratingInstance, field: 'value')}" valueMessagePrefix="rating.value"/>

</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'areaOfLife', 'error')} required">
	<label for="areaOfLife">
		<g:message code="rating.areaOfLife.label" default="Area Of Life" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="areaOfLife" name="areaOfLife.id" from="${com.codeharmony.wheeloflife.AreaOfLife.list()}" optionKey="id" required="" value="${ratingInstance?.areaOfLife?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'isSkip', 'error')} ">
	<label for="isSkip">
		<g:message code="rating.isSkip.label" default="Is Skip" />
		
	</label>
	<g:checkBox name="isSkip" value="${ratingInstance?.isSkip}" />

</div>

