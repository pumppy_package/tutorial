<!DOCTYPE html>
<html lang="en"><head>
    <meta name="layout" content="front"/>

    <title>The Ultimate Wheel of Life Interactive Assessment</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Jumbotron -->
    <div class="jumbotron">
    <div class="container">
        %{--<div id="socialmedia">
            <div id="linkedin" data-url="${createLink(absolute : 'true')}" data-text="LinkedIn Share"></div>
            <div id="twitter" data-url="${createLink(absolute : 'true')}" data-text="Twitter Share"></div>
            <div id="facebook" data-url="${createLink(absolute : 'true')}" data-text="Facebook Share"></div>
            --}%%{--<div id="googleplus" data-url="${createLink(absolute : 'true')}" data-text="GooglePlus Share"></div>--}%%{--
        </div>--}%
    </div>
        <h1>Wheel of Life!</h1>
        <p class="lead">The Wheel of Life is a simple yet powerful tool for visualizing all areas of your life at once to see where you most need improvement. It only takes a minute to complete and it's totally free!</p>
        <p>            <a class="btn btn-primary btn-lg" href="http://player.vimeo.com/video/80629469" data-toggle="lightbox">Watch Video</a>
        <a class="btn btn-lg btn-success" href="${createLink(uri: '/wheel/register')}" role="button">Fill out your Wheel</a></p>

    </div>
        <!-- Video / Generic Modal -->
        <div class="modal fade" id="mediaModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <!-- content dynamically inserted -->
                    </div>
                </div>
            </div>
        </div>

</body></html>