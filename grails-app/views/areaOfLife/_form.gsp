<%@ page import="com.codeharmony.wheeloflife.AreaOfLife" %>



<div class="fieldcontain ${hasErrors(bean: areaOfLifeInstance, field: 'rating', 'error')} ">
	<label for="rating">
		<g:message code="areaOfLife.rating.label" default="Rating" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${areaOfLifeInstance?.rating?}" var="r">
    <li><g:link controller="rating" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="rating" action="create" params="['areaOfLife.id': areaOfLifeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'rating.label', default: 'Rating')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: areaOfLifeInstance, field: 'color', 'error')} required">
	<label for="color">
		<g:message code="areaOfLife.color.label" default="Color" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="color" required="" value="${areaOfLifeInstance?.color}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: areaOfLifeInstance, field: 'highlight', 'error')} required">
	<label for="highlight">
		<g:message code="areaOfLife.highlight.label" default="Highlight" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="highlight" required="" value="${areaOfLifeInstance?.highlight}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: areaOfLifeInstance, field: 'label', 'error')} required">
	<label for="label">
		<g:message code="areaOfLife.label.label" default="Label" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="label" required="" value="${areaOfLifeInstance?.label}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: areaOfLifeInstance, field: 'projects', 'error')} ">
	<label for="projects">
		<g:message code="areaOfLife.projects.label" default="Projects" />
		
	</label>
	

</div>

