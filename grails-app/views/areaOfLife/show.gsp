
<%@ page import="com.codeharmony.wheeloflife.AreaOfLife" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'areaOfLife.label', default: 'AreaOfLife')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-areaOfLife" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-areaOfLife" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list areaOfLife">
			
				<g:if test="${areaOfLifeInstance?.rating}">
				<li class="fieldcontain">
					<span id="rating-label" class="property-label"><g:message code="areaOfLife.rating.label" default="Rating" /></span>
					
						<g:each in="${areaOfLifeInstance.rating}" var="r">
						<span class="property-value" aria-labelledby="rating-label"><g:link controller="rating" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${areaOfLifeInstance?.color}">
				<li class="fieldcontain">
					<span id="color-label" class="property-label"><g:message code="areaOfLife.color.label" default="Color" /></span>
					
						<span class="property-value" aria-labelledby="color-label"><g:fieldValue bean="${areaOfLifeInstance}" field="color"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${areaOfLifeInstance?.highlight}">
				<li class="fieldcontain">
					<span id="highlight-label" class="property-label"><g:message code="areaOfLife.highlight.label" default="Highlight" /></span>
					
						<span class="property-value" aria-labelledby="highlight-label"><g:fieldValue bean="${areaOfLifeInstance}" field="highlight"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${areaOfLifeInstance?.label}">
				<li class="fieldcontain">
					<span id="label-label" class="property-label"><g:message code="areaOfLife.label.label" default="Label" /></span>
					
						<span class="property-value" aria-labelledby="label-label"><g:fieldValue bean="${areaOfLifeInstance}" field="label"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${areaOfLifeInstance?.projects}">
				<li class="fieldcontain">
					<span id="projects-label" class="property-label"><g:message code="areaOfLife.projects.label" default="Projects" /></span>
					
						<g:each in="${areaOfLifeInstance.projects}" var="p">
						<span class="property-value" aria-labelledby="projects-label"><g:link controller="project" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:areaOfLifeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${areaOfLifeInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
