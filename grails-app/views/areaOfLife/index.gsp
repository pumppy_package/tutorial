
<%@ page import="com.codeharmony.wheeloflife.AreaOfLife" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'areaOfLife.label', default: 'AreaOfLife')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-areaOfLife" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-areaOfLife" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="color" title="${message(code: 'areaOfLife.color.label', default: 'Color')}" />
					
						<g:sortableColumn property="highlight" title="${message(code: 'areaOfLife.highlight.label', default: 'Highlight')}" />
					
						<g:sortableColumn property="label" title="${message(code: 'areaOfLife.label.label', default: 'Label')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${areaOfLifeInstanceList}" status="i" var="areaOfLifeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${areaOfLifeInstance.id}">${fieldValue(bean: areaOfLifeInstance, field: "color")}</g:link></td>
					
						<td>${fieldValue(bean: areaOfLifeInstance, field: "highlight")}</td>
					
						<td>${fieldValue(bean: areaOfLifeInstance, field: "label")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${areaOfLifeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
