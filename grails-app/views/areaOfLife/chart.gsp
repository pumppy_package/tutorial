<%@ page import="grails.converters.JSON" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>



</head>
<body>

<canvas id="skills" width="300" height="300"></canvas>

<script type="application/javascript">

    var data = ${raw(areaOfLifeInstance)}


    var context = document.getElementById('skills').getContext('2d');

    var skillsChart =  new Chart(context).PolarArea(data, {
//                responsive:true,
        animationEasing : "easeOutBounce"
    });
</script>

</body>


</html>
