package com.codeharmony.wheeloflife

import com.google.common.collect.Lists
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.transaction.Transactional

@Secured(['permitAll'])
class WheelController {

    def messagingService
    def PDFGeneratorService

    @Secured(['permitAll'])

   def index(){

        def projectInstance = Project.findByUrl(params?.id)

       if(projectInstance?.status == 'Complete'){
            redirect(action: 'mywheel', params: [id: projectInstance])
            return
        }

        if(!projectInstance){
            flash.message = "Please fill up following form!"
            redirect(action: 'register')
            return
        }

        if(projectInstance?.status == 'New'){
            session.areaOfLife = (Lists.newLinkedList(AreaOfLife.list()))
            projectInstance?.status = 'In Progress'
            if(!projectInstance.save(flush: true)){
                flash.message = "Something went wrong please try again later."
                redirect(action: 'register')
                return
            }
        }

        println session.areaOfLife?.label

        String jsonProjectInstance = Rating.findAllByProjectAndIsSkip(projectInstance, false) as JSON

        def ratingAreaInstance

        if(session.areaOfLife){
            ratingAreaInstance = session.areaOfLife.get(0)
        }

        if(!session.areaOfLife){
            projectInstance?.status = 'Complete'
            if(projectInstance.save(flush: true)){
                redirect(action: 'mywheel', params: [id: projectInstance])
                return
            }
        }

        [ratingAreaInstance:ratingAreaInstance, projectInstance:projectInstance, jsonProjectInstance:jsonProjectInstance]
    }

    @Secured(['permitAll'])
    def saveAsPDF(){

        def projectInstance = Project.findByUrl(params?.id)
        String dataURI = params.imageUrl
        String imageDataBase64String = dataURI.split('base64,')[1]

        String fileLocation = grailsApplication.config.wheeloflife.pdflocation

        PDFGeneratorService.saveWheelOfLifeToPDF(fileLocation, projectInstance, imageDataBase64String)

        render file: new File (fileLocation  + projectInstance?.url + ".pdf"), fileName: 'my_wheel_of_life.pdf'

    }

    def addRatingToAreaOfLife(){
        def areaOfLifeInstance = AreaOfLife.get(params?.ratingAreaInstance)
        def projectInstance = Project.findByUrl(params?.projectInstance)
        if(!areaOfLifeInstance ||  projectInstance?.status == 'Complete'){
            redirect(action: 'mywheel', params: [id: projectInstance])
            return
        }
        def rating
        if(!projectInstance.areaOfLifes.contains(areaOfLifeInstance)){
            areaOfLifeInstance.addToProjects(projectInstance)

            if(!areaOfLifeInstance.save(flush: true)){
                flash.message = "Something went wrong please try again later!"
                return
            }



            if(params?.id == 'isskip'){
                rating = new Rating(value: 0, areaOfLife:areaOfLifeInstance, project: projectInstance, isSkip: true )
            } else{
                rating = new Rating(value: params?.id, areaOfLife:areaOfLifeInstance, project: projectInstance )
            }
        }


        if(rating?.save(flush: true)){
            def removeIndex
            session.areaOfLife?.eachWithIndex{area,i ->

                if(area.label == areaOfLifeInstance?.label){
                    removeIndex = i
                    //session.areaOfLife.remove(i)
                }
            }
            println removeIndex
            if(removeIndex!=null){
                session.areaOfLife.remove(removeIndex)
            }

        }

        redirect(action: 'index', params: [id: projectInstance])
    }

    @Secured(['permitAll'])
    def register(){

    }

    def mywheel(){
        def projectInstance = Project.findByUrl(params?.id)
        def projectLink =createLink(action:'mywheel',id:projectInstance.url, absolute: true)
        String jsonProjectInstance = Rating.findAllByProjectAndIsSkip(projectInstance, false) as JSON
        /*if(messagingService.sendWheelOfLife(projectInstance, projectLink)){
            projectInstance?.emailSent=true
            projectInstance.save(flush:true)
        }*/
        [jsonProjectInstance:jsonProjectInstance, projectInstance: projectInstance]
    }

    def triggerEmail(){
        def projectInstance = Project.findByUrl(params?.id)
        def projectLink =createLink(action:'mywheel',id:projectInstance.url, absolute: true)
        String jsonProjectInstance = Rating.findAllByProjectAndIsSkip(projectInstance, false) as JSON

        if(projectInstance && projectInstance.emailSent==false) {
            String dataURI = params.imageUrl
            String imageDataBase64String = dataURI.split('base64,')[1]

            String fileLocation = grailsApplication.config.wheeloflife.pdflocation


            PDFGeneratorService.saveWheelOfLifeToPDF(fileLocation, projectInstance, imageDataBase64String)
            if(messagingService.sendWheelOfLife(projectInstance, projectLink,fileLocation  + projectInstance?.url + ".pdf")){
                projectInstance?.emailSent=true
                projectInstance.save(flush:true)
            }

        }


        render "SUCCESS"
        return
    }

    @Secured(['permitAll'])
    @Transactional
    def save(){

        def areaOfLife = session["areaOfLife"]
        session["areaOfLife"] = AreaOfLife.list()

        if(!params?.firstName && !params?.email && !params?.lastName){
            flash.message = "One or more required fields are missing."
            redirect(action: 'register')
        }

        def url = Project.generateUrl()
        def projectInstance = new Project(url: url, createdDate: new Date(), lastUpdated: new Date())

        def visitor = new Visitor(firstName: params?.firstName, lastName: params?.lastName, email: params?.email)

        visitor?.project = projectInstance

        if(!visitor.save(flush: true)){
            flash.message = "Something went wrong please try again later!"
            redirect(action: 'register')
        } else{
            redirect(action: 'index', params:[id: projectInstance])
        }


    }
}
