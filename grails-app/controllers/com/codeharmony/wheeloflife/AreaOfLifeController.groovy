package com.codeharmony.wheeloflife

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)

@Secured(['ROLE_ADMIN'])
class AreaOfLifeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AreaOfLife.list(params), model:[areaOfLifeInstanceCount: AreaOfLife.count()]
    }

    def show(AreaOfLife areaOfLifeInstance) {
        respond areaOfLifeInstance
    }

    def create() {
        respond new AreaOfLife(params)
    }

    def jsonAreaOfLife = {
        def areaOfLifeInstance = AreaOfLife.list()
        render areaOfLifeInstance.encodeAsJSON()
    }

    def chart(){
        String areaOfLifeInstance = AreaOfLife.list() as JSON
        [areaOfLifeInstance:areaOfLifeInstance]

    }

    @Transactional
    def save(AreaOfLife areaOfLifeInstance) {
        if (areaOfLifeInstance == null) {
            notFound()
            return
        }

        if (areaOfLifeInstance.hasErrors()) {
            respond areaOfLifeInstance.errors, view:'create'
            return
        }

        areaOfLifeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'areaOfLife.label', default: 'AreaOfLife'), areaOfLifeInstance.id])
                redirect areaOfLifeInstance
            }
            '*' { respond areaOfLifeInstance, [status: CREATED] }
        }
    }

    def edit(AreaOfLife areaOfLifeInstance) {
        respond areaOfLifeInstance
    }

    @Transactional
    def update(AreaOfLife areaOfLifeInstance) {
        if (areaOfLifeInstance == null) {
            notFound()
            return
        }

        if (areaOfLifeInstance.hasErrors()) {
            respond areaOfLifeInstance.errors, view:'edit'
            return
        }

        areaOfLifeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AreaOfLife.label', default: 'AreaOfLife'), areaOfLifeInstance.id])
                redirect areaOfLifeInstance
            }
            '*'{ respond areaOfLifeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AreaOfLife areaOfLifeInstance) {

        if (areaOfLifeInstance == null) {
            notFound()
            return
        }

        areaOfLifeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AreaOfLife.label', default: 'AreaOfLife'), areaOfLifeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'areaOfLife.label', default: 'AreaOfLife'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
