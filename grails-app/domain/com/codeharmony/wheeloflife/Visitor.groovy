package com.codeharmony.wheeloflife

class Visitor {

    String firstName
    String lastName
    String email

    static hasOne = [project:Project]

    static constraints = {
        firstName blank: false, nullable: false
        lastName blank: false, nullable: false
        email blank: false, nullable: false, email: true
        project blank:true, nullable: true
    }
}
