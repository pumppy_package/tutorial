package com.codeharmony.wheeloflife

import org.apache.commons.lang.RandomStringUtils

class Project {

    String url
    Date createdDate
    Date lastUpdated
    boolean emailSent=false

    String status='New'

    static hasMany = [areaOfLifes:AreaOfLife, rating:Rating]

    static belongsTo = [visitor:Visitor]

    static constraints = {
    }

    def beforeUpdate() {
        lastUpdated = new Date()
    }

    def beforeValidate() {
        if (id == null) {
            createdDate = new Date()
            lastUpdated = new Date()
        }
    }

    def static generateUrl(){
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        Integer length = 9
        return RandomStringUtils.random(length, charset.toCharArray())
    }

    String toString(){
        return url
    }
}
