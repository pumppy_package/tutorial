package com.codeharmony.wheeloflife

class AreaOfLife {

    String color
    String highlight
    String label

    static hasMany = [projects: Project, rating: Rating]

    static belongsTo = Project

    static constraints = {

        rating nullable: true, blank:true
    }
}
