package com.codeharmony.wheeloflife

class Rating {

    Integer value
    boolean isSkip = false
    static belongsTo = [areaOfLife:AreaOfLife, project:Project]

    static constraints = {
        project unique: 'areaOfLife'
        value(inList: 0..10)
    }
}
