import com.codeharmony.wheeloflife.AreaOfLife
import com.codeharmony.wheeloflife.Project
import com.codeharmony.wheeloflife.Rating
import com.codeharmony.wheeloflife.Role
import com.codeharmony.wheeloflife.User
import com.codeharmony.wheeloflife.UserRole
import grails.converters.JSON

class BootStrap {

    def init = { servletContext ->

        JSON.registerObjectMarshaller(Rating){
            def output = [:]

            output['color'] = it?.areaOfLife?.color
            output['label'] = it?.areaOfLife?.label
            output['highlight'] = it?.areaOfLife?.highlight
            output['value'] = it?.value

            return output
        }

        def adminUser = new User(username: 'wl_admin', password: 'wl_chinita')
        def adminRole = new Role(authority: 'ROLE_ADMIN')

        if(!Role.findByAuthority('ROLE_ADMIN')){
            adminRole.save(flush: true)
        }

        if(!User.findByUsername('admin')){
            if (adminUser.save(flush: true)) {
                UserRole.create adminUser, adminRole, true
            }
        }

        def countAreaOfLife = AreaOfLife.count()
        if(countAreaOfLife==0){
            new AreaOfLife(label: 'Fun', color: '#2E4272', highlight: '#2E4272').save(flush: true)
            new AreaOfLife(label: 'Career', color: '#226666', highlight: '#226666').save(flush: true)
            new AreaOfLife(label: 'Personal Growth', color: '#582A72', highlight: '#582A72').save(flush: true)
            new AreaOfLife(label: 'Money', color: '#609732', highlight: '#609732').save(flush: true)
            new AreaOfLife(label: 'Health', color: '#882D61', highlight: '#882D61').save(flush: true)
            new AreaOfLife(label: 'Friends', color: '#811D1D', highlight: '#811D1D').save(flush: true)
            new AreaOfLife(label: 'Family', color: '#81741D', highlight: '#81741D').save(flush: true)
            new AreaOfLife(label: 'Relationship', color: '#D39B5E', highlight: '#D39B5E').save(flush: true)
        }
    }
    def destroy = {

    }
}
